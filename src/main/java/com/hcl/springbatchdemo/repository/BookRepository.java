package com.hcl.springbatchdemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hcl.springbatchdemo.entity.Book;

public interface BookRepository extends JpaRepository<Book,Integer >{

}
