package com.hcl.springbatchdemo.config;

import org.springframework.batch.item.ItemProcessor;

import com.hcl.springbatchdemo.entity.Book;

public class BookProcessor implements ItemProcessor<Book, Book> {

	@Override
	public Book process(Book book) throws Exception {
		return book;
	}
}
