package com.hcl.springbatchdemo.config;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.job.builder.JobBuilder;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.step.builder.StepBuilder;
import org.springframework.batch.item.data.RepositoryItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;
import org.springframework.transaction.PlatformTransactionManager;

import com.hcl.springbatchdemo.entity.Book;
import com.hcl.springbatchdemo.repository.BookRepository;

import lombok.AllArgsConstructor;

@Configuration
@AllArgsConstructor
public class SpringBatchConfig {
	private BookRepository bookRepository;

	@Bean
	public FlatFileItemReader<Book> reader() {
		FlatFileItemReader<Book> itemReader = new FlatFileItemReader<>();
		itemReader.setResource(new FileSystemResource("src/main/resources/bookadatas.csv"));
		itemReader.setName("csvReader");
		itemReader.setLinesToSkip(1);
		itemReader.setLineMapper(lineMapper());
		return itemReader;
	}

	private LineMapper<Book> lineMapper() {
		DefaultLineMapper<Book> lineMapper = new DefaultLineMapper<>();
		DelimitedLineTokenizer lineTokenizer = new DelimitedLineTokenizer();
		lineTokenizer.setDelimiter(",");
		lineTokenizer.setStrict(false);
		lineTokenizer.setNames("id", "bookName", "bookAuthor");

		BeanWrapperFieldSetMapper<Book> fieldSetMapper = new BeanWrapperFieldSetMapper<>();
		fieldSetMapper.setTargetType(Book.class);

		lineMapper.setLineTokenizer(lineTokenizer);
		lineMapper.setFieldSetMapper(fieldSetMapper);
		return lineMapper;
	}

	@Bean
	public BookProcessor processor() {
		return new BookProcessor();
	}

	@Bean
	 RepositoryItemWriter<Book> writer() {
		RepositoryItemWriter<Book> writer = new RepositoryItemWriter<>();
		writer.setRepository(bookRepository);
		writer.setMethodName("save");
		return writer;
	}

	@Bean
	 Step step1(JobRepository jobRepository, PlatformTransactionManager platformTransactionManager) {
		return new StepBuilder("csv-step", jobRepository).<Book, Book>chunk(10, platformTransactionManager)
				.reader(reader()).processor(processor()).writer(writer()).taskExecutor(taskExecutor()).build();
	}

	@Bean
	 Job runJob(JobRepository jobRepository, PlatformTransactionManager transactionManager) {
		return new JobBuilder("importStudents", jobRepository).flow(step1(jobRepository, transactionManager)).end()
				.build();
	}

	@Bean
	public TaskExecutor taskExecutor() {
		SimpleAsyncTaskExecutor asyncTaskExecutor = new SimpleAsyncTaskExecutor();
		asyncTaskExecutor.setConcurrencyLimit(10);
		return asyncTaskExecutor;
	}
}
